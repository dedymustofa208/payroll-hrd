<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubDepartment extends Model
{
    protected $guard    = 'id';
    protected $table    = 'sub_department';
    protected $fillable = ['department_id','sub_department_name','deleted_at','created_by','updated_by'];
    protected $dates    = ['created_at','updated_at'];

    function deptDetail(){
        return $this->belongTo('App\Models\Department','id','department_id');
    }
}
