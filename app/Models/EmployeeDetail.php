<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class EmployeeDetail extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guard    = 'id';
    protected $table    = 'master_employee_detail';
    protected $fillable = ['employee_id','no_ktp','no_bpjs','npwp','no_rekening','kode_pos','alamat','no_telp','no_telp_darurat','catatan','deleted_by','deleted_at','created_by','updated_by'];
    protected $dates    = ['created_at','updated_at'];

    function detail(){
        return $this->hasMany('App\Models\Employee','id','employee_id');
    }
}
