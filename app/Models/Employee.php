<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Employee extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guard    = 'id';
    protected $table    = 'master_employee';
    protected $fillable = ['employee_name','photo','position','level','department_id','sub_department_id','join_date','leave_date','deleted_by','deleted_at','created_by','updated_by'];
    protected $dates    = ['created_at','updated_at'];

    function header(){
        return $this->belongsTo('App\Models\EmployeeDetail','id','employee_id');
    }
}
