<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guard    = 'id';
    protected $table    = 'department';
    protected $fillable = ['department_name','deleted_at','created_by','updated_by'];
    protected $dates    = ['created_at','updated_at'];

    function deptHeader(){
        return $this->hasMany('App\Models\SubDepartment','id','department_id');
    }
}
