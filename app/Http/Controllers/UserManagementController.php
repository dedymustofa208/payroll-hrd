<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Storage;
use Image;
use Log;
use Validator;
use App\Models\User;
use Carbon\Carbon;


class UserManagementController extends Controller
{
    public function index(){
        return view('administrator.index');
    }

    public function permission_v(){
        return view('administrator.v_permission');
    }

    public function role_v(){
        return view('administrator.v_roles');
    }

    public function storeData(Request $request){
        $validator = Validator::make($request->all(), [
            'username'      => 'required|max:255',
            'email'         => 'required|email',
            // 'password_user' => 'required|min:6',
            // 'photo_user'    => 'required|image|mimes:jpeg,png,jpg,png|max:2048'
        ]);
    
        if ($validator->fails()) {
            return response()->json($validator->errors(),500);
        }else{
            try {
                DB::beginTransaction();
                    // $file = $request->file('photo');
                    // $image = Image::make($request->file('photo'))->resize(300, 200);
                    // // encode the image as a JPEG with 60% quality and save to storage
                    // $image->encode('jpg', 60);
                    // $filename = $file->getClientOriginalName();
                    // // Store the image in the storage path
                    // Storage::put('app/public/' . $filename, $image->stream());
    
                    // // Get the URL to the stored image
                    // $url = Storage::url('app/public/' . $filename);
                    User::firstOrCreate([
                        'name'              => trim($request->get('username')),
                        'password'          => bcrypt(trim($request->get('password_user'))),
                        'email_verified_at' => Carbon::now(),
                        'email'             => trim($request->get('email')),
                        // 'photo_user'     => $url
                    ]);
                DB::commit();
                return response()->json('User created successfully',200);
            } catch (\Exception $e) {
                DB::rollback();
                Log::error($e->getMessage());
                return response()->json('Failed to create user', 500);
            }
        }
    }
}
