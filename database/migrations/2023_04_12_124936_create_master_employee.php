<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_employee', function (Blueprint $table) {
            $table->char('id')->primary();
            $table->string('employee_name')->nullable();
            $table->string('photo')->nullable();
            $table->string('position')->nullable();
            $table->string('level')->nullable();
            $table->integer('department_id')->nullable();
            $table->integer('sub_department_id')->nullable();
            $table->date('join_date')->nullable();
            $table->date('leave_date')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_employee');
    }
}
