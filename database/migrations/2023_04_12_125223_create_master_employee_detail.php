<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterEmployeeDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_employee_detail', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('employee_id',36)->nullable();
            $table->text('no_ktp')->nullable();
            $table->text('no_bpjs')->nullable();
            $table->text('npwp')->nullable();
            $table->text('no_rekening')->nullable();
            $table->string('kode_pos')->nullable();
            $table->text('alamat')->nullable();
            $table->text('no_telp')->nullable();
            $table->text('no_telp_darurat')->nullable();
            $table->text('catatan')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_employee_detail');
    }
}
