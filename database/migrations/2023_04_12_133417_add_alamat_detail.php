<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAlamatDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_employee_detail', function (Blueprint $table) {
            $table->text('rt')->nullable();
            $table->text('rw')->nullable();
            $table->text('desa')->nullable();
            $table->text('kecamatan')->nullable();
            $table->text('kabupaten')->nullable();
            $table->text('kota')->nullable();
            $table->string('negara')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_employee_detail', function (Blueprint $table) {
            //
        });
    }
}
