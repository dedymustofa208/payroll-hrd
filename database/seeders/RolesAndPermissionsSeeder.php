<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        // create permissions
        Permission::create(['name' => 'master-user-management']);
        Permission::create(['name' => 'master-employee']);
        Permission::create(['name' => 'master-department']);

        // create roles and assign created permissions

        // this can be done as separate statements
        $role = Role::create(['name' => 'dragma']);
        $user = User::find(1);
        $user->assignRole($role);
    }
}
