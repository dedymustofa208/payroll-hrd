<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Employee;
use App\Models\EmployeeDetail;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();
            $new = Employee::firstOrcreate([
                'employee_name'     => 'Dedy Setiyadi Mustofa',
                'position'          => 'staff',
                'level'             => 'L0',
                'department_id'     => '1',
                'sub_department_id' => '1',
                'join_date'         => Carbon::now()->format('Y-m-d'),
                'created_by'        => '1'
            ]);
    
            EmployeeDetail::firstOrcreate([
                'employee_id'       => $new->id,
                'no_ktp'            => Crypt::encrypt('3322033119300001'),
                'no_bpjs'           => Crypt::encrypt('12345'.Carbon::now()->format('u')),
                'npwp'              => Crypt::encrypt('67891'.Carbon::now()->format('u')),
                'no_rekening'       => Crypt::encrypt('723452653'),
                'kode_pos'          => '507777',
                'desa'              =>  Crypt::encrypt('susukan'),
                'rt'                =>  Crypt::encrypt('03'),
                'rw'                =>  Crypt::encrypt('01'),
                'kecamatan'         =>  Crypt::encrypt('susukan'),
                'kabupaten'         =>  Crypt::encrypt('semarang'),
                'kota'              => null,
                'negara'            => 'indonesia',
                'alamat'            => Crypt::encrypt('Dsn.Susukan RT.03/RW.01 , Ds.Susukan, Kec.Susukan, Kab.Semarang'),
                'no_telp'           => Crypt::encrypt('628985505663'),
                'no_telp_darurat'   => Crypt::encrypt('6281326586979'),
                'created_by'        => '1'
            ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
