<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon; 
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'                  => 'admin',
            'email'                 => 'admin@gmail.com',
            'email_verified_at'     => Carbon::now(),
            'password'              => bcrypt('12345'),
        ]);   
    }
}
