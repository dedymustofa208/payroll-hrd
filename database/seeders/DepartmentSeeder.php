<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Department;
use App\Models\SubDepartment;
use Carbon\Carbon;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = Department::create([
            'department_name'       => 'production',
            'created_by'            => '1'
        ]);

        SubDepartment::create([
            'department_id'         => $new->id,
            'sub_department_name'   => 'produksi 1',
            'created_by'            => '1'
        ]);
    }
}
