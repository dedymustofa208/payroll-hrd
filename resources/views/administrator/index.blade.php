@extends('layouts.app')


@section('page-header')
<div class="content-header pt-0 pl-0 pr-0">
    <div class="container-fluid p-0">
        <div class="card card-body pb-0 pt-2">
            <div class="row mb-2">
                
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">User Management</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">User Registration</h3>
            </div>
            <div class="col-md-6">
                <form id="regist-form">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan Nama Anda" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email Anda" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="password_user" name="password_user" placeholder="Masukkan Password" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="photo_user" name="photo_user" accept="image/png, image/jpeg, image/jpg">
                                    <label class="custom-file-label" for="exampleInputFile">Upload Foto Anda</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="button" id="btn-save" class="btn btn-outline-primary">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function(){
    document.getElementById('btn-save').addEventListener('click', function(e) {
        e.preventDefault();
        var csrfToken = $('meta[name="csrf-token"]').attr('content');
        var form = document.getElementById('regist-form');
        var formData = new FormData(form);
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't save this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, save it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': csrfToken
                    }
                });
                $.ajax({
                    url: '{{ route("users.storeData") }}',
                    method: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    beforeSend: function(xhr) {
                        // Code to execute before sending the AJAX request
                    },
                    success: function(response) {
                        console.log(response);
                        Swal.fire({
                            icon: 'success',
                            title: 'Good!',
                            text: 'Data Saved!',
                            timer: 1500
                        });
                    },
                    error: function(xhr, status, error, response) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                            timer:3500
                        });
                    }
                });
            }
        });
    });
});
</script>
@endsection