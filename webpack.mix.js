const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
.vue()
.styles('resources/plugins/fontawesome-free/css/all.min.css','public/css/all.min.css')
.styles('resources/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css','public/css/tempusdominus-bootstrap-4.min.css')
.styles('resources/plugins/icheck-bootstrap/icheck-bootstrap.min.css','public/css/icheck-bootstrap.min.css')
.styles('resources/dist/css/adminlte.min.css','public/css/adminlte.min.css')
.styles('resources/plugins/overlayScrollbars/css/OverlayScrollbars.min.css','public/css/OverlayScrollbars.min.css')
.styles('resources/plugins/daterangepicker/daterangepicker.css','public/css/daterangepicker.css')
.styles('resources/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.css','public/css/bootstrap-4.css')

.scripts('resources/dist/js/adminlte.min.js','public/js/adminlte.min.js')
.scripts('resources/plugins/sweetalert2/sweetalert2.all.js','public/js/sweetalert2.all.js')
.scripts('resources/plugins/jquery-ui/jquery-ui.min.js','public/js/jquery-ui.min.js')
.scripts('resources/plugins/jquery/jquery.min.js','public/js/jquery.min.js')
.scripts('resources/plugins/bootstrap/js/bootstrap.bundle.min.js','public/js/bootstrap.bundle.min.js')
.scripts('resources/plugins/chart.js/Chart.min.js','public/js/Chart.min.js')
.scripts('resources/plugins/jquery-knob/jquery.knob.min.js','public/js/jquery.knob.min.js')
.scripts('resources/plugins/moment/moment.min.js','public/js/moment.min.js')
.scripts('resources/plugins/daterangepicker/daterangepicker.js','public/js/daterangepicker.js')
.scripts('resources/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js','public/js/tempusdominus-bootstrap-4.min.js')
.scripts('resources/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js','public/js/jquery.overlayScrollbars.min.js')
.scripts('resources/dist/js/adminlte.js','public/js/adminlte.js')
.js('node_modules/jquery/dist/jquery.js', 'public/js')
.copyDirectory('resources/dist/img','public/images')
.copyDirectory('resources/plugins/fontawesome-free/webfonts','public/webfonts')

.sass('resources/sass/app.scss', 'public/css');

